﻿$().ready(function() {
	$("#candidates_list, #voted_list").sortable({
		connectWith: ".connectedSortable",
		containment: "#candidates",
		dropOnEmpty: true,
		helper: "clone",
		opacity: 0.6,
		revert: true
	}).disableSelection();
	$("#voted_list").css("height", $("#candidates_list").height()+"px");
	
	// al click, muovi il candidato in fondo alla lista dei votati
	$("#candidates_list li").click(function(e) {
		if (this.parentNode.id == "voted_list") return;
		var element = $(this);
		var newParent = $("#voted_list");
		var oldOffset = element.offset();
		element.appendTo(newParent);
		var newOffset = element.offset();

		var temp = element.clone().appendTo('body');
		temp.addClass('candidate')
			.css('position', 'absolute')
			.css('left', oldOffset.left)
			.css('top', oldOffset.top)
			.css('zIndex', 1000);
		element.hide();
		temp.animate( {'top': newOffset.top, 'left':newOffset.left}, 'normal', function(){
		   element.show();
		   temp.remove();
		});
	});

	$("#cast_ballot").submit(function(e) {
		if ($("select.ballot_candidate").length) {
			// Versione LoFi
			var tot = 0;
			var els = $("select.ballot_candidate");
			for (i = 0; i < els.length; i++) {
				if (els.eq(i).val()) { $tot++; }
			}
			if (!tot)  {
				e.preventDefault();
				alert("Devi votare almeno un candidato... selezionali in ordine di preferenza utilizzando i selettori a discesa");				
				return false;
			}
			return true;
		} 

		var voti = $("#voted_list li");
		var tot = voti.length;
		if (!tot) {
			alert("Devi votare almeno un candidato... trascina i nomi dalla colonna di sinistra a quella di destra. se vuoi votare scheda bianca, usa il pulsante apposito");
			e.preventDefault();
			return false;
		}

		for (var i = 0; i < tot; i++) {
			var el = voti.eq(i);	
			var ii = i+1;
			$("#id_vote_"+ii).val(el.attr("id").replace("candidate_",""));
			$("#id_label_"+ii).val(el.html());
		};
		return true;
	});
	$("#auth_form").submit(function(e) { 
		if (!$("#id_last_name").val() || !$("#id_code").val()) { 
			alert("Devi inserire sia il tuo cognome che il codice di voto che hai ricevuto via email!");
			e.preventDefault();
			return false;
		}
	});
	/*
	$( ".message" ).dialog({
		modal: true,
		buttons: {
			Chiudi: function() { $( this ).dialog( "close" ); }
		}
	});
	*/
});

