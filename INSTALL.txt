FARE VOTO
Piattaforma di voto online per Fare per Fermare il Declino
(C) 2013 Filippo Caprioli - Rilasciato sotto licenza GPL 2.0
============================================================


L'installazione richiede un normale LAMP stack, con PHP e MySQL.

E' richiesto 
- MySQL 5.5, con supporto utf8 e tabelle ARCHIVE attive
- Server SSL
- il supporto alle sessioni PHP (sconsigliato memcache)
- La distribuzione include un file .htaccess per proteggere i file sensibili su apache.


Per installare
  1. copiare i file in una cartella del web server
  2. rinominare config.example.php in config.php e modificare la configurazione
  3. importare farevoto.sql nel DB MySQL
  
