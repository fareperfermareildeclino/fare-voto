<?php
/*
This file is part of FareVoto.
Copyright (C) 2013 Filippo Caprioli - Fare per Fermare il Declino

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
version 2 as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, If not, see <http://www.gnu.org/licenses/>
*/

error_reporting(E_ERROR | E_PARSE);
define("FAREVOTO", True);
/* Impedisci al browser e ai proxy di cacheare la pagina */
header("Cache-Control: no-cache, must-revalidate");
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");

/* Richiedi una connessione https */
if(!$_SERVER["HTTPS"] && $_SERVER["HTTP_X_FORWARDED_PROTO"]	!= "https") { 
	header("Location: https://".$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"]); 
	die(__("Connessione SSL richiesta"));
}
require_once("common.php");
$sess = session_start();
if (!$sess) { critical(__("Impossibile aprire la sessione di lavoro")); }
if (!$_SERVER["REMOTE_ADDR"]) critical(__("Errore nella configurazione del server - non riesco a trovare il tuo IP"));
define("UNSUPPORTED_BROWSER", (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 6.') !== FALSE && !isset($_COOKIE["FareVotoUnsupportedBrowser"])));


/*
* Index.php agisce come router+view: imposta il flusso dell'app in base allo status della sessione
*/

if (isset($_POST["do_logout"]) && $_POST["do_logout"]) { 
	$_SESSION = array(); 
	if (isset($_POST["session_complete"])) { 
		$_SESSION["Messages"] = array(__("Grazie per aver votato! La tua sessione &egrave; ora conclusa")); 
	}
}
/*
* Fase di autenticazione e validita' del ticket di voto
*/ 
$ticket = null;
if (!isset($_SESSION["TicketID"]) || !$_SESSION["TicketID"]) {
	/* Autenticazione */
	$code = (isset($_REQUEST["code"])) ? trim($_REQUEST["code"]) : "";
	$last_name =  (isset($_REQUEST["last_name"])) ? trim($_REQUEST["last_name"]) : "";
	
	if ($_POST["code"] AND $code and $last_name) {
		/* Prima controlla che non siano stati effettuati piu' di 10 login falliti da quell'ip o per quell'utente/codice */		
		#$login_attempts = ticket_check_failed_logins($_SERVER["REMOTE_ADDR"], $code, $last_name);
		$login_attempts = 1;
		if ($login_attempts > 10) { 
			critical(__("Hai sbagliato troppi login - per motivi di sicurezza il tuo codice &egrave; stato temporaneamente bloccato. Ti preghiamo di contattare il supporto per farlo sbloccare, oppure riprovare fra circa un'ora.")); 
		}
		
		if (constant("DEBUG")) {
			$ticket = create_debug_ticket($_POST);
		} else {
			$ticket = dbget("SELECT * FROM poll_tickets WHERE code=".e(hash_password($code))." AND last_name=".e($last_name)."");
		}
		
		if ($ticket AND $ticket["id"]) {
			poll_user_log("AUTH_OK", $last_name);
			$_SESSION["TicketID"] = $ticket["id"];
			$_SESSION["FirstName"] = $ticket["first_name"];
			$_SESSION["LastName"] = $ticket["last_name"];
			$_SESSION["Email"] = $ticket["email"];
			$_SESSION["Messages"] = $_SESSION["ErrorMessages"] = array();
			$_SESSION["LoFi"] = False;

		} else {
			poll_user_log("AUTH_FAILED", $last_name);
			$_SESSION["ErrorMessages"][] = __("Codice di voto  o cognome errato. Verificali entrambi e riprova, oppure contatta il supporto.");
		}
	}
	if (!$ticket) {
		$_SESSION = array("Messages"=>$_SESSION["Messages"], "ErrorMessages"=>$_SESSION["ErrorMessages"]);
		show_auth($code, $last_name);
		exit;
	}
} else {
	$ticket = dbget("SELECT * FROM poll_tickets WHERE id=".e($_SESSION["TicketID"]));	
}
/* Sanity check sul ticket di voto */
if (!$ticket || !$_SESSION["TicketID"]) { critical(__("Sessione di voto non valida. Contatta il supporto: ".HELPDESK_CONTACT)); }

/* Configura e verifica la scadenza del ticket */
if ($ticket["start_date"] && !is_numeric($ticket["start_date"])) {
	$ticket["start_date"] = strtotime($ticket["start_date"]);
}
$_SESSION["TicketStartDate"] = $ticket["start_date"];

if ($ticket["validity"] && $ticket["start_date"] && ($ticket["start_date"]+$ticket["validity"]) < time()) {
	critical(__("Sessione di voto scaduta - hai impiegato troppo tempo per votare. Contatta il supporto per far estendere il tempo a tua disposizione."));	
} 

/* Attiva/disattiva versione accessibile */
if (constant("UNSUPPORTED_BROWSER")) {
	$_SESSION["LoFi"] = True;
} else if (isset($_POST["enable_lofi"])) {
	if ($_POST["enable_lofi"] == 1) { $_SESSION["LoFi"] = True; }
	else if ($_POST["enable_lofi"] == 2) { $_SESSION["LoFi"] = False; }
}


/*
* Controlli di sicurezza passati: passa alla selezione della votazione
*/ 

if (!isset($_REQUEST["poll_id"]) || !$_REQUEST["poll_id"]) {
	/* Scegli a quale votazione partecipare */
	show_select_ballot($_SESSION["TicketID"]);
	exit;
	
} else if ($_REQUEST["poll_id"]) {	
	/*
	* Voto vero e proprio
	*/ 

	$SQL = "SELECT p.*, t.* FROM poll_polls AS p INNER JOIN poll_polls_tickets AS t ON p.id = t.poll_id AND t.ticket_id=".e($_SESSION["TicketID"])." WHERE p.id=".e(trim($_REQUEST["poll_id"]));
	$poll = dbget($SQL);
	$now = time();
	if (!$poll) { 
		error_redirect(__("Votazione non trovata!"), null);
	}
	else if ($poll["has_voted"]) { 
		error_redirect(__("Hai gi&agrave; votato per la ".$poll["name"].". Scegli una votazione diversa!")); 
	}
	else if (strtotime($poll["start_date"]) > $now) { 
		error_redirect(__('La votazione per la '.$poll["name"].' non &egrave; ancora aperta. Aprir&agrave; il '.display_date($poll["start_date"]).' e chiuder&agrave; il '.display_date($poll["end_date"]))); 
	}
	else if (strtotime($poll["end_date"]) < $now) { 
		error_redirect(__('La votazione "'.$poll["name"].'" &grave; chiusa dal '.$poll["end_date"])); 
	}

	/* votazione attiva e ticket valido. Procedi. */
	if (!$_POST["vote_1"] && !$_POST["vote_2"] && !$_POST["vote_3"] && !$_POST["blank_ballot"]) {
		/* Elenco dei candidati */
		if ($_SESSION["LoFi"]) {
			show_accessible_ballot($poll);
		} else {
			show_ballot($poll);
		}
	} else {
		if (!isset($_POST["confirm_vote"]) || !$_POST["confirm_vote"]) {
			/* Mostra schermata di conferma */
			if (isset($_POST["blank_ballot"])) {
				show_confirm_blank_ballot($poll, $_POST);  
			} else { 
				show_confirm_ballot($poll, $_POST);  
			}
		} else {
			/* Registra il voto */
			cast_ballot($poll, $_POST);
			if (!$_SESSION["TicketStartDate"]) {
				/* Fai partire il timer di durata del ticket dalla prima votazione di cui mostriamo i candidati */
				dbquery("UPDATE poll_tickets SET start_date=NOW(), ip=".e($_SERVER["REMOTE_ADDR"])." WHERE id=".e($_SESSION["TicketID"]));
				$_SESSION["TicketStartDate"] = $now;
			}			
			$_SESSION["Messages"][] = __("Grazie per aver partecipato alla votazione de ".$poll["name"]."! Il tuo voto &egrave; stato registrato dal sistema. Ora scegli la prossima votazione a cui vuoi partecipare.");
			header('Location: index.php');
		}
	}

	
} else {
	critical(__("Errore nel routing interno dell'applicazione di voto"));
}


session_commit();

