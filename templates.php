<?php
/*
This file is part of FareVoto.
Copyright (C) 2013 Filippo Caprioli - Fare per Fermare il Declino

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
version 2 as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, If not, see <http://www.gnu.org/licenses/>
*/


if (!defined("FAREVOTO")) { die("Security error"); }
	
/*
* Layout stuff
*/
function show_header($header_payoff="", $page_title="") {
	if (defined("HEADER_SHOWED")) return;
	define("HEADER_SHOWED", True);
	if (constant("DEBUG")) $header_payoff .= " DEMO DI PROVA";
	$head_title = ($page_title) ? $page_title : $header_payoff;
	
?><!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title><?php echo $head_title ?> | Congressi 2013</title>
	<!--<meta name="viewport" content="width=device-width">-->
	<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400,600" media="all" />
	<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Rambla:400,700" media="all" />
	<link rel="stylesheet" href="normalize.css">
	<link rel="stylesheet" href="main.css">
    <script src="jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="jquery-ui.min.js" type="text/javascript"></script>
	<script src="jquery.ui.touch-punch.min.js" type="text/javascript"></script>
	<script src="farevoto.js" type="text/javascript"></script>
</head>

<body>
<div id="bodycnt">
  <header><div id="header"><div class="wrapper">
    <a href="index.php"><img src="logo.png"></a>	
	<h1 id="page_title">Votazioni</h1>
	<h2 id="page_payoff"><?php echo $header_payoff; ?></h2>
	
	<?php if (isset($_SESSION["TicketID"]) && $_SESSION["TicketID"]) { ?>
		<form method="POST" action="index.php" id="user_title">
		  <?php echo $_SESSION["FirstName"]." ".$_SESSION["LastName"] ?><br>
		  <input type="hidden" name="do_logout" value="1">
		  <input type="submit" value="Esci">
		</form>	  
	<?php } ?>
  </div></div></header>
  <div class="wrapper main">  

<?php
	if ($page_title) echo "<h1>$page_title</h1>";
	
	if (isset($_SESSION["Messages"]) && $_SESSION["Messages"]) { 
		message(join("<p>", $_SESSION["Messages"]));
		$_SESSION["Messages"] = array();
	}
	if (isset($_SESSION["ErrorMessages"]) && $_SESSION["ErrorMessages"]) { 
		error(join("<p>", $_SESSION["ErrorMessages"]));
		$_SESSION["ErrorMessages"] = array();
	}
}

function show_footer() {
	if (defined("FOOTER_SHOWED")) return;
	define("FOOTER_SHOWED", True);
	if (constant("DEBUG")) { echo "<h2 style=\"padding-top: 30px; text-align: center;\"><u>ATTENZIONE: il sistema di voto &egrave; in modalit&agrave; dimostrativa. I VOTI RACCOLTI NON SONO VALIDI</u></h2>"; }
?>

  </div><!-- chiusura wrapper -->
</div><!-- chiusura bodycnt -->
  <footer><div id="footer"><div class="wrapper">	
	<?php if (isset($_SESSION["TicketID"]) && $_SESSION["TicketID"] && !constant("UNSUPPORTED_BROWSER")) { ?>
	<form method="POST" id="enable_lofi">
		<input type="hidden" name="enable_lofi" value="<?php echo ($_SESSION["LoFi"]) ? "2" : "1"; ?>">
		<input type="submit" value="<?php echo (isset($_SESSION["LoFi"]) && $_SESSION["LoFi"]) ? __("Torna alla versione normale") : __("Passa alla la versione semplificata"); ?>">
	</form>  
	<?php } ?>
	<?php echo HELPDESK_CONTACT ?>	  	 
   </div></div></footer>
   <!--[if lte IE 6]>
	<script type="text/javascript">document.cookie='FareVotoUnsupportedBrowser=1";</script>
<![endif]-->
</body>
</html>
<?php
}


function show_auth($code, $last_name) {
	show_header();
?>
	<form method="POST" id="auth_form">
		<p></p>
		<label>Il tuo cognome<br><input type="text" name="last_name" id="id_last_name" value="<?php echo htmlentities($last_name); ?>"></label>
		<label>La tua password di voto<br><input type="text" name="code"  id="id_code" value="<?php echo htmlentities($code); ?>" max_length="8"></label>
		<input type="submit" class="submit" value="Accedi alle votazioni">
	</form>
<?php
	show_footer();
}


function show_select_ballot($ticket_id) {
	global $TZ;
	$SQL = "SELECT t.*, p.*  FROM poll_polls_tickets AS t INNER JOIN poll_polls AS p ON t.poll_id = p.id AND t.ticket_id=".e($ticket_id)." WHERE t.ticket_id=".e($ticket_id)." ORDER BY has_voted, position, name";
	$result = dbquery($SQL);	
	$now = time(); 
	$has_voted = $can_vote = $will_vote = 0;
	
	$html = '<ol id="select_ballot">';
	while ($poll = $result->fetch_array(MYSQLI_ASSOC)) {
		$is_active = (strtotime($poll["start_date"]) < $now && strtotime($poll["end_date"]) > $now) ? TRUE : FALSE;		
		$start_date = display_date("d/m/Y H:i", $poll["start_date"]);
		$end_date = display_date("d/m/Y H:i", $poll["end_date"]);
		
		$url = "index.php?poll_id=".$poll["id"];
		if ($poll["has_voted"])	{	
			$has_voted++;
			$html .= '<li class="has_voted"><a>'.$poll["name"].'<br><em>'.$poll["description"].'</em><br><br><span><strong>Hai gi&agrave; votato</strong> - lo spoglio inizier&agrave; il '.$end_date.'</span></a></li>';
		}
		else if ($is_active) { 
			$can_vote++;
			$html .= '<li class="active"><a href="'.$url.'">'.$poll["name"].'<br><em>'.$poll["description"].'</em><br><br><span><b>Puoi votare</b> - Urne aperte dal '.$start_date.' al '.$end_date.'</a></li>';
		}
		else { 
			$will_vote++;
			$html .= '<li class="inactive"><a>'.$poll["name"].'<br><em>'.$poll["description"].'</em><br><br><span><b>Votazione Chiusa</b> - <em>Urne aperte dal '.$start_date.' al '.$end_date.'</em></span></a></li>';
		}
	}
	$html .= '</ol>';
	
	/* Condizioni speciali:  ha gia' votato in tutto, segnalalo all'utente, sovrascrivendo eventuali altri messaggi */
	if (!$can_vote && !$will_vote && $has_voted) { 
		$_SESSION["Messages"] = array(__('Hai votato in tutte le elezioni. Puoi uscire dal sistema cliccando sul tasto "Esci" qui sotto'), '<center><form method="POST" id="form_logout"><input type="hidden" name="do_logout" value="1"><input type="hidden" name="session_complete" value="1"><input type="submit" class="submit" value="Esci e concludi la votazione"></form></center>');
	}
	
	show_header(__("A quale votazione vuoi partecipare?"), __("Scegli la votazione a cui vuoi partecipare"));
	echo $html;
	show_footer();
}


function show_ballot($poll) {
	show_header($poll["name"]);
	$SQL = "SELECT c.*, p.* FROM poll_polls_candidates AS p LEFT JOIN poll_candidates AS c ON p.candidate_id = c.id WHERE p.poll_id=".e($poll["id"])." ORDER BY c.last_name ASC";
	$result = dbquery($SQL);
	$candidates_list = "";
	$tot = 0;
	while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
		$label = escape($row["first_name"].' '.$row["last_name"]);
		$subtext = $row["party"];
		$candidates_list .= '<li id="candidate_'.$row["id"].'" class="candidate">'.$label.'<em>'.$subtext.'</em></li>';
		$tot++;
	}
	if (!$tot) { critical(__("Possibile errore interno: non ci sono candidati per questa votazione!")); }
?>
   <hgroup>
	<h1><?php echo $poll["name"] ?></h1>
	<h2><?php echo $poll["description"] ?></h2>
   </hgroup>
   <p class="spaced">Per votare devi mettere in ordine i tuoi candidati preferiti. Clicca sui loro nomi, oppure trascinali nella colonna destra. 
   Puoi modificare in qualsiasi momento l'ordine dei candidati che intendi votare semplicemente trascinando e riordinando i loro nomi.<br>
   Inoltre, non sei obbligato a selezionare tutti i candidati.
   Quando hai finito, clicca sul pulsante "vota per questa lista di candidati".</p>
<?php if ($poll["party_lock"]) { ?>
<p class="spaced bold" style="color: red">Ti ricordiamo che devi inserire come primo nome un candidato che sostiene <?php echo $poll["party_lock"] ?>. Altrimenti, il sistema ignorer� i voti ad esponenti di altre liste.</p>
<?php } ?>
	
  <div id="candidates">
  <h3>I candidati fra cui scegliere</h3> 
  <h3 style="text-align: right">I candidati che vuoi votare (in ordine di preferenza)</h3>
  <hr />
  <ol id="candidates_list" class="connectedSortable"><?php echo $candidates_list; ?></ol>
  <div id="arrow">&gt;&gt;</div>
  <ol id="voted_list" class="connectedSortable">
  </ol>
  <hr/>
  </div>
  <hr>

  
  <form id="cast_ballot" method="POST">  
    <input type="hidden" name="poll_id" value="<?php echo $poll["id"]; ?>">
<?php
	for ($i = 1; $i <= $tot; $i++) {
		echo "    <input type=\"hidden\" id=\"id_vote_$i\" name=\"vote_$i\" value=\"\">\n";
		echo "    <input type=\"hidden\" id=\"id_label_$i\" name=\"label_$i\" value=\"\">\n";		
	}
?>
    <center><input type="submit" class="submit" value="Vota questa lista di candidati"></center>
  </form>
  <br>
  <form method="POST" id="blank_ballot" class="clearfix">
	<input type="hidden" name="blank_ballot" value="1">
	<center><input type="submit" class="grey submit" value="Vota Scheda Bianca (Astieniti)"></center>
  </form>
  <br><br>
  <center><a class="btn grey" href="javascript:location.reload(true);">Mi sono confuso - Ricomincia il voto</a></center>

  <hr/>
<?php
  show_footer();
}

function show_accessible_ballot($poll) {
	show_header($poll["name"]);
	$SQL = "SELECT c.*, p.* FROM poll_polls_candidates AS p LEFT JOIN poll_candidates AS c ON p.candidate_id = c.id WHERE p.poll_id=".e($poll["id"])." ORDER BY c.last_name ASC";
	$result = dbquery($SQL);
	$select = '<option value="">----------</option>\n';
	$tot = 0;
	while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
		$tot++;
		$label = escape($row["first_name"].' '.$row["last_name"]);
		$select .= '<option value="'.$row["id"].'|||'.$label.'">'.$label.'</option>\n';
	}
	if (!$tot) { critical(__("Possibile errore interno: non ci sono candidati per questa votazione!")); }
?>
   <hgroup>
	<h1><?php echo $poll["name"] ?></h1>
	<h2><?php echo $poll["description"] ?></h2>
   </hgroup>
   <p>Per votare devi mettere in ordine i tuoi candidati preferiti.  Non sei obbligato a selezionare tutti i candidati.
   Quando hai finito, clicca sul pulsante "vota per questa lista di candidati" per inviare il tuo voto.</p>
   
	<form method="POST" id="cast_ballot" class="lofi">	
	<ol>
<?php
	for ($i = 0; $i < $tot; $i++) {
		$ii = $i+1;
		echo "\t\t".'<li><label>Scegli un candidato<br><select name="vote_'.$ii.'" class="ballot_candidate" id="ballot_candidate_'.$ii.'">'.$select.'</select></label></li>';
	}
?>
	</ol>
	<br>
	<input type="submit" class="submit" value="Vota per questi candidati">
</form>
<form method="POST" id="blank_ballot"  class="lofi">
	<input type="hidden" name="blank_ballot" value="1">
	<input type="submit" class="grey submit" value="Vota Scheda Bianca (Astieniti)">
</form>

<?php

	show_footer();
}

function show_confirm_blank_ballot($poll, $post) {
	show_header($poll["name"]);
?>
   
	<form method="POST" id="form_confirm_blank_ballot">
	  <input type="hidden" name="blank_ballot" value="1">
	  <input type="hidden" name="poll_id" value="<?php echo $poll["id"]; ?>">
	  <input type="hidden" name="confirm_vote" value="1">
	  <h1>Conferma il tuo voto: Scheda Bianca</h1>
	  <p>Sicuro di voler votare bianca per l'elezione de <?php echo $poll["name"]; ?>?</p>
	  <input type="submit" value="Confermo: voglio votare scheda bianca" class="spaced submit">
	  <a class="btn grey" href="index.php?poll_id=<?php echo $poll["id"]; ?>">Torna Indietro e cambia il tuo voto</a>
	</form>
<?php
	show_footer();
}

function show_confirm_ballot($poll, $post) {
	show_header($poll["name"]);
?>
	<form method="POST" id="form_confirm_ballot">
		<h1><?php echo $poll["name"] ?> - conferma il tuo voto</h1>
		<p>Prima di registrare il tuo voto, ti preghiamo di verificare che sia tutto a posto; se l'ordine delle tue preferenze &egrave; corretto clicca su "conferma voto", altrimenti su "torna indietro".</p>
		<input type="hidden" name="poll_id" value="<?php echo $poll["id"]; ?>">
		<input type="hidden" name="confirm_vote" value="1">
		<ol id="confirm_ballot">
<?php
	$tot = count($post);
	$ii = 0;
	$party_lock = strtolower($poll["party_lock"]);
	for ($i = 1; $i <= $tot; $i++) {
		if (!isset($post["vote_$i"])) { continue; }
		$val = trim($post["vote_$i"]);
		if (!$val) { continue; }			
		$val = explode("|||", $val);
		$label = (isset($post["label_$i"])) ? $post["label_$i"] : trim($val[1]);
		$vote = trim($val[0]);
		if (!$vote) { continue; }
		if ($party_lock) {
			# La prima scelta deve essere di una certa lista. Altrimenti vengono discarded
			$u = dbget("SELECT party FROM poll_candidates WHERE id=".e($vote));
			if (strtolower($u["party"]) != $party_lock) { 
				error("Devi indicare come prima scelta un candidato che supporti $party_lock"); 
				break; 
			}
			$party_lock = '';
		}
		$ii++;
		echo '<li><input type="hidden" name="vote_'.$ii.'" value="'.$vote.'">'.$label.'</li>';
	}
?>
		</ol>
		<center>
			<?php if ($ii) { ?><input type="submit" value="Conferma e salva il tuo voto" class="spaced submit"><?php } ?>
			<br><br>
			<a class="btn grey" href="index.php?poll_id=<?php echo $poll["id"]; ?>">Torna Indietro e cambia il tuo voto</a>
		</center>
	</form>
<?php
	show_footer();
}  


