<?php
/*
This file is part of FareVoto.
Copyright (C) 2013 Filippo Caprioli - Fare per Fermare il Declino

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
version 2 as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, If not, see <http://www.gnu.org/licenses/>
*/


if (!defined("FAREVOTO")) { die("Security error"); }
require_once("config.php");
if (!$CONFIG) { die("Configurazione non caricata"); }
require_once("templates.php");
require_once("Mandrill.php");

/*
* General func
*/
function __($msg) {
	return $msg;
}

function display_date($format="", $date=null){	
	global $TZ;
	$datetime = new DateTime($date);
	$datetime->setTimezone($TZ);
	if (!$format) { $format = "d/m/Y H:i"; }
	return $datetime->format($format);
}

function escape($txt) {
	return htmlentities($txt);
}

function generate_random_string($length = 24) {
    $characters = '123456789abcdefghjkmnpqrstuvwxyz';
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $randomString;
}

function hash_password($str) {
	// Password are uppercased/case insensitive for user-friendliness
	return sha1(strtoupper(FAREVOTO_HASHTOKEN.$str));
}


function critical($err, $log_error="") {
	global $CONFIG; 
	if (constant("DEBUG") && $log_error) $err = $err.$log_error;
	if (php_sapi_name() !== 'cli') { 
		show_header();	
		echo '<div class="error message">'.$err.'<br><br>'.HELPDESK_CONTACT.'</div>';
		show_footer();
	} else {
		echo $err;
	}
	
	if ($log_error && constant("LOGFILE_PATH")) {
		$fp = fopen(LOGFILE_PATH, "a");
		if ($fp) {
			flock($fp, LOCK_EX);
			fwrite($fp, "[".date("Y-m-d H:i:s")."] ".$log_error."\n");
			flock($fp, LOCK_UN);
			fclose($fp);
		}
	}
	die();
}
function error_redirect($err, $redirect) {
	$_SESSION["ErrorMessages"][] = $err;
	if (!$redirect) $redirect = "index.php";
	header("Location: $redirect");
	die();
}

function error($err) {	
    echo '<div class="error message">'.$err.'<br><br>'.HELPDESK_CONTACT.'</div>';
}

function message($msg) {
    echo '<div class="message">'.$msg.'</div>';
}

function warn($msg) {
    echo '<div class="warn message">'.$msg.'</div>';
}

/*
* DB stuff
*/

function dbconnect() {
	global $CONFIG, $db;
	$db = new mysqli($CONFIG["db"]["host"], $CONFIG["db"]["user"], $CONFIG["db"]["password"], $CONFIG["db"]["db"]);

	/* check connection */
	if ($db->connect_errno) { 	
		critical(__("Errore nella connessione al database"), "DB Error:  [".$db->connect_errno."] ".$db->connect_error);
		
	}
	if (!$db->set_charset("utf8")) {
		critical(__("Errore nella connessione al database"), "DB Error:  [".$db->errno."] ".$db->error); 
	}
	$db->query("SET timezone='+00:00'");
	$db->select_db($CONFIG["db"]["db"]);	
	return $db;
}
  
function dbquery($SQL) {
	global $db;
	if (!$db) { $db = dbconnect(); }
	$res = $db->query($SQL);
	if ($res===false) {
		critical(__("Errore nel database"), "Invalid query: ".$db->error."\nWhole query: ".$SQL);
	}
	return $res;
}

function e($param) {
	global $db;
	if (!$db) $db = dbconnect();
	if (!$param) return "''";
	if (is_array($param)) {
		$res = array(); $tot = count($param);
		foreach ($param as $p) {
			$res[] = "'".$db->real_escape_string($p)."'";
		}		
		return join(", ", $res);
	}
	else if (is_int($param) || is_float($param)) { return $param; }
	return "'".$db->real_escape_string($param)."'";
}

function dbget($SQL) {
	/* Ritorna la prima regola dei risultati */
	global $db;
	if (!$db) { $db = dbconnect(); }
	$q = $db->query($SQL);	
	if (($q = $db->query($SQL))===false ) {
		critical(__("Errore nel database"), "Invalid query: ".$db->error."\nWhole query: ".$SQL);
	}
	$row = $q->fetch_array(MYSQLI_ASSOC);
	$q->close();
	return $row;
}
	



/*
* La funzione principale, che registra il voto sul database
*/
function cast_ballot($poll, $post) {
	global $db;
	$db->autocommit(False);
	$log_data = json_encode($_POST);
	$is_debug = (intval(constant("DEBUG"))) ? "1" : "0";
	$ballot_id = md5($_SESSION["TicketID"].$_SESSION["FirstName"].$_SESSION["LastName"].rand(1,1000000000));

	// Registra il dato del POST in un log, per crosscheck successivo 	
	if ($poll["secret_ballot"]) {
		$SQL_LOG = 'INSERT INTO poll_votes_log (poll_id, ballot_data, is_debug) VALUES ('.e($poll["id"]).','.e($log_data).', '.e($is_debug).')';
	} else {
		$SQL_LOG = 'INSERT INTO poll_votes_log (ticket_id, first_name, last_name, ip, date, poll_id, ballot_data, is_debug) VALUES '.
		' ('.e($_SESSION["TicketID"]).', '.e($_SESSION["FirstName"]).', '.e($_SESSION["LastName"]).', '.e($_SERVER["REMOTE_ADDR"]).', NOW(), '.e($poll["id"]).', '.e($log_data).', '.e($is_debug).')';
	}
	dbquery($SQL_LOG);
	$tot = count($post);
	
	/* Crea a mano una query mysql multi-insert, per la massima efficienza */
	if ($poll["secret_ballot"]) {
		$SQL = 'INSERT INTO poll_votes (poll_id, ballot_id, candidate_id, position, is_debug, is_blank) VALUES ';
	} else {
		$SQL = 'INSERT INTO poll_votes (poll_id, ballot_id, candidate_id, position, date, ticket_id, first_name, last_name, is_debug, is_blank) VALUES ';
	}
	
	$SQLData = array();
	/* In caso di scheda bianca */
	if ($post["blank_ballot"]) {
		if ($poll["secret_ballot"]) { 
			$SQLData[] = "(".e($poll["id"]).", ".e($ballot_id).", NULL, NULL, ".$is_debug.", 1)";
		} else {
			$SQLData[] = "(".e($poll["id"]).", ".e($ballot_id).", NULL, NULL, NOW(), ".e($_SESSION["TicketID"]).", ".e($_SESSION["FirstName"]).", ".e($_SESSION["LastName"]).", ".e($is_debug).", 1)";
		}
	} else {	
		/* Scheda regolare */
		for ($i = 1; $i <= $tot; $i++) {
			if (!isset($post["vote_$i"])) { continue; }
			$val = $post["vote_$i"];
			$val = ($val) ? e($val) : "NULL";
			if ($poll["secret_ballot"]) { 
				$SQLData[] = "(".e($poll["id"]).", ".e($ballot_id).", $val, ".e($i).", ".e($is_debug).", 0)";
			} else {
				$SQLData[] = "(".e($poll["id"]).", ".e($ballot_id).", $val, ".e($i).", NOW(), ".e($_SESSION["TicketID"]).", ".e($_SESSION["FirstName"]).", ".e($_SESSION["LastName"]).", ".e($is_debug).", 1)";
			}
		}
	}

	if (!$SQLData) { critical(__("Errore nei dati di voto inviati: risulta tu non abbia votato alcun candidato, n� scheda bianca! Torna indietro e riprova.")); }
	dbquery($SQL.join(", ", $SQLData)); 
	
	/* Aggiora lo status di voto */
	dbquery('UPDATE poll_polls_tickets SET has_voted=1 WHERE poll_id='.e($poll["id"]).' AND ticket_id='.e($_SESSION["TicketID"]));
	$db->commit();
	$db->autocommit(True);
	
	/* invia una ricevuta di voto all'email dell'utente */
	$mail_text = <<<EOT
Grazie per aver votato, $_SESSION[FirstName] $_SESSION[LastName]

con la presente ti confermiamo che abbiamo registrato il tuo voto nell'elezione
$poll[poll_name]

Grazie a nome di Fare
EOT;
	mail();
	$mandrill = new Mandrill(MANDRILL_API_KEY);
	$message = array(
		'subject' => '[FARE] Grazie per aver votato',
		'from_email' => 'info@fermareildeclino.it',
		'text'=> htmlentities($mail_text),
		'to' => array(array('email' => $_SESSION["Email"], 'name' => $_SESSION["FirstName"]." ".$_SESSION["LastName"]))
	);
	$mandrill->messages->send($message);
	
	return True;
}


function ticket_check_failed_logins($ip, $code, $last_name) { 
	$SQL = "SELECT COUNT(*) AS failed_logins FROM poll_user_log WHERE date > DATE_SUB(NOW(), INTERVAL 1 HOUR) AND action='LOGIN_FAILED' AND (ip=".e($ip)." OR (last_name=".e($last_name).") OR  (code=".e($last_name)."))";
	$tot = dbget($SQL);
	return intval($tot["failed_logins"]);
}

function poll_user_log($action, $last_name="", $code="") { 
	if (!$action) { critical("FareVoto: errore interno al programma"); }
	$SQL = 'INSERT INTO poll_user_log (date, action, last_name, code, ip, url, post, server, cookie) VALUES ('.
	join(", ", array('NOW()', e($action),  e($last_name), e($code), e($_SERVER["REMOTE_ADDR"]), e($_SERVER["REQUEST_URI"]), e(json_encode($_POST)), e(json_encode($_SERVER)), e(json_encode($_COOKIE)) )).
	")";
	dbquery($SQL);

}


function create_debug_ticket($data) {
	/* Solo se la piattaforma � in modalit� demo, crea dei ticket a ogni login */
	global $db;
	if (!constant("DEBUG")) die(__("Questa funzione pu� essere usata solo in modalita' demo"));
	$code = "DEBUG_".generate_random_string(10);
	$SQL = "INSERT INTO poll_tickets (code, first_name, last_name, email, validity, is_debug) VALUES (".e($code).", '', ".e($data["last_name"]).", 'strumenti@fermareildeclino.it', 3600, 1)";
	dbquery($SQL);
	$tid = $db->insert_id;
	if (!$tid) { critical("Errore interno nel generare il ticket di voto di prova"); }
	$ticket = dbget("SELECT * from poll_tickets WHERE id=".e($tid));
	if (!$ticket) { critical("Errore di coerenza interna nel generare il ticket di voto di debug"); }
	
	$SQLData = array();
	$result = dbquery("SELECT id FROM poll_polls");	
	while ($row = $result->fetch_array(MYSQLI_ASSOC)) {	
		$SQLData[] = "(".$row["id"].", ".$tid.", 0)";
	}
	dbquery("INSERT INTO poll_polls_tickets VALUES ".join(",",$SQLData));

	return $ticket;
	
};
