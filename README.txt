===========================================================
FARE VOTO
Piattaforma di voto online per Fare per Fermare il Declino

Copyright (C) 2013 Filippo Caprioli - Fare per Fermare il Declino
Rilasciato sotto licenza GPL 2.0
============================================================

La webapp replica in tutto e per tutto le logiche di un voto segreto fisico,
e non consente la tracciabilit� in caso di voto segreto.



ARCHITTETTURA DEI FILE
========================

index.php 
	svolge funzione di router, ed � l'unico punto di accesso riconosciuto dell'app.
	Si occupa dell'autenticazione, e quindi invia alla funzione preferita

common.php 
	contiene librerie di base e di astrazione
	
templates.php 
	contiene le funzioni che generano l'html (molto semplice)
	
send_poll_emails.shell.php
	utility da linea di comando che invia i codici di voto (in progress)
	
	

IL FLUSSO
========================
Il flusso � in 3 fasi
	1) Autorizzazione e redemption del ticket di voto (cognome+codice)
	2) Selezione della elezione a cui si vuole votare
	3) Selezione, in ordine, dei candidati che si intende votare
	4) Conferma del voto
	5) Ritorno alla schermata 2)
   
   
   
LO STILE
========================
La scelta del procedurale piuttosto che OO � voluta: l'app � molto semplice, e in questo modo, 
partendo da index.php, � sicuramente pi� facile poterla verificare per programmatori di qualsiasi livello.
Keep It Simple And Stupid.


   
LOGGING
========================   
I vari passaggi vengono registrati in log separati fra loro, di vario tipo (da tabelle di DB write-only in formato ARCHIVE, a file di testo fisici)
Nel voto segreto non deve essere possibile incrociare fra loro questi log per ricostruire chi ha votato cosa.

   
   
TODO
========================

* Completare e testare il sistema via mandrill che invia le email coi codici di voto
* Invio via email di una ricevuta di voto (dubbio: dovr� contenere o meno il voto espresso in caso di voto segreto? Secondo me si)
* Pagina con elaborazione dei risultati di voto

* Restyle grafico, compreso CSS responsivo
* Realizzare un backend per inserire i dati via web invece che via sql

