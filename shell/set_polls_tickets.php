<?php
/*
This file is part of FareVoto.
Copyright (C) 2013 Filippo Caprioli - per Fare per Fermare il Declino

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
version 2 as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, If not, see <http://www.gnu.org/licenses/>
*/
if (php_sapi_name() !== 'cli') { die("Questo script pu� essere invocato solo dalla linea di comando"); }
define("FAREVOTO", True);
chdir(dirname(__FILE__)."../");
require_once("common.php");
chdir(dirname(__FILE__));


$result = dbquery("SELECT * FROM poll_tickets WHERE start_date IS NULL");
while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
	if (!$row["province"]) die("Errore interno");
	$res = dbquery("SELECT id FROM poll_polls WHERE FIND_IN_SET(".e($row["province"]).", provinces)");
	$done = 0;
	while ($r = $res->fetch_array(MYSQLI_ASSOC)) {
		dbquery("INSERT IGNORE INTO poll_polls_tickets VALUES (".e($r["id"]).",".e($row["id"]).",0)");
		$done++;
	}
	if (!$done) die("ERRORE INTERNO: nessuno trovato ".print_r($row, True));
	print $row["id"]." => $done\n";
	if ($done < 3) die("ERRORE INTERNO: elezioni insufficienti. Provincia sbagliata? ".print_r($row, True));
}
	



