<?php
/*
This file is part of FareVoto.
Copyright (C) 2013 Filippo Caprioli - per Fare per Fermare il Declino

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
version 2 as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, If not, see <http://www.gnu.org/licenses/>
*/
if (php_sapi_name() !== 'cli') { die("Questo script pu&ograve; essere invocato solo dalla linea di comando"); }
define("FAREVOTO", True);
chdir(dirname(__FILE__)."../");
require_once(dirname(__FILE__)."/../Mandrill.php");
require_once(dirname(__FILE__)."/../common.php");
chdir(dirname(__FILE__));

$options = getopt("p:m:");
$poll_sql = array();
if ($options) {
	if (isset($options["m"]) && $options["m"]) {
		$poll_sql[] = "t.email IN (".e($options["m"]).")";
	}
	if (isset($options["p"]) && $options["p"]) {
		$poll_sql[] = "t.email_sent=0  AND pt.poll_id IN (".e($options["p"]).")";
	}
} else {
	$poll_sql[] = "t.email_sent=0 ";
}

$SQL = "SELECT t.*, p.*, pt.* FROM poll_tickets AS t JOIN poll_polls_tickets AS pt ON t.id = pt.ticket_id JOIN poll_polls AS p ON pt.poll_id = p.id WHERE ".join(" AND ", $poll_sql);
#if (constant("DEBUG")) $SQL .= " AND t.id=92457";
$SQL .= " GROUP BY t.id";
if (constant("DEBUG_TEST_EMAIL")) $SQL .= " LIMIT 1";
$result = dbquery($SQL);
$done = 0;
$mandrill = new Mandrill(MANDRILL_API_KEY);

while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
	$password = strtoupper(generate_random_string(6));
	dbquery("UPDATE poll_tickets SET code=".e(hash_password($password)).", email_sent=email_sent+1 WHERE id=".$row["ticket_id"]);
	$start_date =  display_date("d/m/Y H:i", $row["start_date"]);
	$end_date =  display_date("d/m/Y H:i", $row["end_date"]);

	$last_name = urlencode(strtolower($row["last_name"]));
	$rcpt_email = (constant("DEBUG_TEST_EMAIL")) ? DEBUG_TEST_EMAIL : $row["email"];
	
	$subject = "[Fare] Il tuo codice per votare";
	$mail_html = <<<EOT
<html>
<body style="background-color: #F2F2F2;  font-family: sans-serif">
<div style="margin: 10px auto; width: 560px; padding: 20px; background-color: #FFF;">
<p>Caro Tesserato di <b style="color: #c8051d">Fare per Fermare il Declino</b>,<br>
grazie alla password contenuta in questa email potrai votare alle elezioni interne di Fare, da domenica 5 maggio a martedi 7 maggio. Conservala con cura!</p>
<p>In particolare, sei chiamato a votare per
  <ul>
  <li>i <b>delegati</b> che a loro volta eleggeranno il presidente e la direzione nazionale l' 11-12 maggio</li>
  <li>il <b>presidente</b> del coordinamento <b>della tua regione</b></li>
  <li>la <b>direzione regionale</b> che supporter&agrave; il presidente</li>
 </ul>
 

<p>Il voto avviene esclusivamente per via elettronica, ed &egrave; possibile votare con qualsiasi computer o tablet connesso a internet.
L'ordine delle preferenze &egrave; importante: il sistema di voto adottato infatti assegna i seggi in base all'ordine dei candidati.
I risultati delle elezioni verranno pubblicati al pi&ugrave; presto mercoledi 8 sul sito fermareildeclino.it, e comunicati via email.</p>

<p>Ti ricordiamo, infine, che <b>il voto &egrave; individuale e segreto: comunicare la vostra password a terze persone per farle votare al tuo posto potrebbe causare l'annullamento del voto</b>.
Non comunicare la tua password ad altri in nessun caso.</p>

<p style="margin: 1.5em; font-size: 1em; white-space: pre; padding: 1em; background-color: #F2F2F2;">
PASSWORD PER VOTARE: <b style="font-size: 1.1em">$password</b>

APERTURA DELLE VOTAZIONI: <b style="font-size: 1.1em">$start_date</b> (ora italiana)
CHIUSURA DELLE VOTAZIONI: <b style="font-size: 1.1em">$end_date</b> (ora italiana)

LINK PER VOTARE:  
<a href="https://www.fermareildeclino.it/farevoto/index.php?last_name=$last_name" target="_blank" style="font-size: 1.2em">https://www.fermareildeclino.it/farevoto/<br>index.php?last_name=$last_name</a>

</p>

<p>La procedura di voto &egrave; facile:
<ol>
  <li>Inserire il proprio cognome e password di voto</li>
  <li>Selezionare la votazione cui si vuole partecipare</li>
  <li>Cliccare, in ordine, sui candidati che si intende votare. <br>
     E' possibile riordinare in qualsiasi momento la lista delle preferenze trascinando il nome col mouse.</li>
  <li>Cliccare su "Vota questa lista di candidati"</li>
  <li>Comparir&agrave; una schermata riassuntiva; verificare che l'ordine sia corretto, e quindi cliccare su "Conferma il voto per questa lista di candidati"</li>
 </ol>
<br>
<p>Dopo aver partecipato a tutte e 3 le votazioni &egrave; necessario cliccare su "esci" per terminare le operazioni di voto.
Dopo aver espresso il primo voto, avrai a disposizione due ore per completare tutte le operazioni di voto.</p>

<p>Per ulteriore assistenza o domande <b style="color: #c8051d">contatta info@fermareildeclino.it</b>, o il referente logistico della tua regione. E ricorda che le votazioni durano fino a martedi: 
dovessi avere dubbi o problemi, puoi riprovare pi&ugrave; tardi dopo aver ottenuto supporto da info@fermareildeclino.it.</p>

<p><b style="color: #c8051d">Grazie e buon voto!</b></p>
</div>
</body>
</html>
EOT;

	/* aggiunta per le DN */
	$subject = "[Fare] Il tuo codice per votare la Direzione Nazionale";
	$party_lock = '';
	if ($row["party_lock"]) {
		$party_lock = '<p><b style="color: #c8051d">Avendo tu espresso pubblicamente il tuo supporto per '.$row["party_lock"].', sei tenuto a inserire un candidato alla DN che supporta '.$row["party_lock"].' come prima scelta nelle tue preferenze. Il sistema di voto ti impedir&agrave; comunque di esprimere un voto errato.</p>';
	}

	$mail_html = <<<EOT
<html>
<body style="background-color: #F2F2F2;  font-family: sans-serif">
<div style="margin: 10px auto; width: 560px; padding: 20px; background-color: #FFF;">
<p>Caro Delegato di <b style="color: #c8051d">Fare per Fermare il Declino</b>,<br>
grazie alla password contenuta in questa email potrai votare alle elezioni della Direzione Nazionale di Fare, venerdi 31. Conservala con cura!</p>
 
<p>Il voto avviene esclusivamente per via elettronica, ed &egrave; possibile votare con qualsiasi computer o tablet connesso a internet.
L'ordine delle preferenze &egrave; importante: il sistema di voto adottato infatti assegna i seggi in base all'ordine dei candidati.</p>


<p>Ti ricordiamo, infine, che <b>il voto &egrave; individuale e pubblico: comunicare la vostra password a terze persone per farle votare al tuo posto potrebbe causare l'annullamento del voto</b>.
Non comunicare la tua password ad altri in nessun caso.</p>

<p style="margin: 1.5em; font-size: 1em; white-space: pre; padding: 1em; background-color: #F2F2F2;">
PASSWORD PER VOTARE: <b style="font-size: 1.1em">$password</b>

APERTURA DELLE VOTAZIONI: <b style="font-size: 1.1em">$start_date</b> (ora italiana)
CHIUSURA DELLE VOTAZIONI: <b style="font-size: 1.1em">$end_date</b> (ora italiana)

LINK PER VOTARE:  
<a href="https://www.fermareildeclino.it/farevoto/index.php?last_name=$last_name" target="_blank" style="font-size: 1.2em">https://www.fermareildeclino.it/farevoto/<br>index.php?last_name=$last_name</a>

</p>

<p>La procedura di voto &egrave; facile:
<ol>
  <li>Inserire il proprio cognome e password di voto</li>
  <li>Selezionare la votazione cui si vuole partecipare</li>
  <li>Cliccare, in ordine, sui candidati che si intende votare. <br>
     E' possibile riordinare in qualsiasi momento la lista delle preferenze trascinando il nome col mouse.</li>
  <li>Cliccare su "Vota questa lista di candidati"</li>
  <li>Comparir&agrave; una schermata riassuntiva; verificare che l'ordine sia corretto, e quindi cliccare su "Conferma il voto per questa lista di candidati"</li>
 </ol>

$party_lock

<p>Per ulteriore assistenza o domande <b style="color: #c8051d">contatta info@fermareildeclino.it</b></p>

<p><b style="color: #c8051d">Grazie e buon congresso!</b></p>
</div>
</body>
</html>
EOT;


	$message = array(
		'subject' => $subject,
		'from_name' => 'Votazioni Fare per Fermare il Declino',
		'from_email' => 'info@fermareildeclino.it',
		'html'=> $mail_html,
		'text'=>null,
		'auto_text' => True,
		'track_opens' => True,
		'track_clicks' => True,
		'important' => True,
		'to' => array(array('email' => $rcpt_email, 'name' => $row["first_name"]." ".$row["last_name"]))
	);
	/*
	if (constant("DEBUG")) {
		var_dump($message);
		continue;
	}
	*/
	$res = $mandrill->messages->send($message, True);
	if ($res && ($res[0]["status"] == "sent" || $res[0]["status"] == "queued")) {
		$done++;
		print $done." => ".$rcpt_email."\n";
		if (constant("DEBUG_TEST_EMAIL")) { var_dump($res); }
	} else {
		print "ERRORE ID ".$row["ticket_id"]." => ".$rcpt_email."\n";
		var_dump($res);
	}

}
echo "\n\nInviate $done email";
	



