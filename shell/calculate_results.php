<?php
/*
This file is part of FareVoto.
Copyright (C) 2013 Filippo Caprioli - per Fare per Fermare il Declino

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
version 2 as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, If not, see <http://www.gnu.org/licenses/>
*/
if (php_sapi_name() !== 'cli') { die("Questo script pu� essere invocato solo dalla linea di comando"); }
define("FAREVOTO", True);
require_once(dirname(__FILE__)."/../common.php");


function rebuild_votes_table() {
	/* Crea una tabella temporanea coi voti raggruppati/GROUP BYiabili per scheda */
	dbquery("TRUNCATE TABLE poll_votes");

	$res = dbquery("SELECT * FROM poll_votes_log");
	$done = 0;
	while ($r = $res->fetch_array(MYSQLI_ASSOC)) {
		$ballot_id = md5($r["ballot_data"].rand(0,100000000000));
		$poll_id = $r["poll_id"];
		$post = json_decode($r["ballot_data"], TRUE);
		$ballot_data = array();
		for ($i = 1; $i <= count($post); $i++) {
			if (!isset($post["vote_$i"])) continue;
			$ballot_data[] = $post["vote_$i"];
		}
		if (isset($post["blank_ballot"])) {
			dbquery("INSERT INTO poll_votes VALUES ($poll_id, '$ballot_id', NULL, NULL, ".e($r["date"]).", ".e($r["ticket_id"]).", ".e($r["first_name"]).", ".e($r["last_name"]).", ".e($r["is_debug"]).", 1 , '', 0)");		
			$done ++;
		} else {
			$tot = count($post);
			for ($i = 1; $i <= $tot; $i++) {
				$val = (isset($post["vote_$i"])) ? $post["vote_$i"] : False;
				if (!$val) { continue; }
				dbquery("INSERT INTO poll_votes VALUES ($poll_id, '$ballot_id', ".e($val).", $i, ".e($r["date"]).", ".e($r["ticket_id"]).", ".e($r["first_name"]).", ".e($r["last_name"]).", ".e($r["is_debug"]).", 0, ".e(join(",",$ballot_data)).", 0)");
			}
		}
		$done ++;
		print "Voto $done fatto\n";
	}	
}

function calculate_alternative_vote($poll) {
	$poll_id = $poll["id"];
	$num_votes = dbget("SELECT COUNT(*) AS tot FROM poll_votes WHERE poll_id=$poll_id AND position = 1");
	$num_votes = intval($num_votes["tot"]);
	$droop = floor($num_votes / 2) + 1;
	
	$elected = $elected_ids = array();
	$excluded = $candidates = array();
	
	while ((!$candidates || max($candidates) < $droop)) {
		# Se sono rimasti solo 2 candidati in lotta, non riassegnare
		if ($candidates AND (count($candidates) - count($excluded)) < 2) { break; }
		$candidates = array();	
		$SQL = ($excluded) ? "AND candidate_id NOT IN (".e($excluded).")" : "";
		$q = dbquery("SELECT candidate_id, COUNT(*) AS votes FROM poll_votes WHERE poll_id=$poll_id AND position=1 $SQL GROUP BY candidate_id ORDER BY votes DESC");
		while ($r = $q->fetch_array(MYSQLI_ASSOC)) {
			$k = $r["candidate_id"];
			$candidates[$k] = intval($r["votes"]);
		}
		
		if ($excluded) {
			$q = dbquery("SELECT candidate_id, COUNT(*) AS votes FROM poll_votes WHERE poll_id=$poll_id AND position=2 AND ballot_id IN (SELECT ballot_id FROM poll_votes WHERE poll_id=$poll_id AND position=1 AND candidate_id IN (".e($excluded).")) GROUP BY candidate_id ORDER BY votes DESC");
			while ($r = $q->fetch_array(MYSQLI_ASSOC)) {
				$k = $r["candidate_id"];
				if (isset($candidates[$k])) {
					$candidates[$k] += intval($r["votes"]);
				} else {
					$candidates[$k] = intval($r["votes"]);
				}	
			}
		}
					
		if (max($candidates) < $droop) {
			#FIXME: gestisci il tie break per eta'			
			$excluded[] = array_search(min($candidates), $candidates);
		}
		/* Se sono solo in 2, non assegnare per esclusione */
		if (count($candidates) <= 2) { break; }		
		
	}
	arsort($candidates);
	$elected = array();
	foreach($candidates as $k => $votes) {
		$elected[] = dbget("SELECT *, $votes AS votes FROM poll_candidates WHERE id=$k");		
	}
	return $elected;

}

function stv_transfer_votes($droop, $poll_id, $candidate_id, $stv_done, $candidates) {
	global $candidate_data;
	$from_user = $candidate_data[$candidate_id];
	$reassign = array();
	if (!$stv_done) $stv_done = array();
	
	# Prendi tutte le schede che hanno come primo voto $candidate_id
	$first_votes = dbget("SELECT COUNT(*) AS votes FROM poll_votes WHERE poll_id=$poll_id AND candidate_id=$candidate_id AND position=1");
	$first_votes = intval($first_votes["votes"]);
	$k = strval($candidate_id);
	$tot_votes = $candidates[$k];
	if ($tot_votes == $droop || $first_votes == $droop) { return array(); }
	
	$q = dbquery("SELECT ballot_data, COUNT(*) AS votes FROM poll_votes WHERE poll_id=$poll_id AND candidate_id=$candidate_id AND position=1 AND transferred_to=0 GROUP BY ballot_data ORDER BY votes DESC");
	#if (array_search($candidate_id, $stv_done) === FALSE)
	$max_reassign = ($first_votes > $droop) ? intval($tot_votes-$droop) : $first_votes;	
	if ($q->num_rows) {
		echo "\n\nCalcolo riassegnazione voti di $from_user[last_name]: $tot_votes voti combinati, $first_votes schede con prima preferenza $max_reassign voti da riassegnare\n";
	}
	
	$reassigned = 0;
	while ($r = $q->fetch_array(MYSQLI_ASSOC)) {
		$votes = intval($r["votes"]);
		# se non raggiungono il droop, redistribuiscile tutte
		if ($tot_votes > $droop) {
			#$quota = intval($votes * ($droop/$tot_votes));
			$quota = ceil($votes * ($droop/$tot_votes));
			$debuglog = "\tquota: $quota = $votes * ($droop/$tot_votes) => ";
		} else {
			$quota = $votes;
			$debuglog = "\tquota: $votes => ";
		}
		
		if (($reassigned + $quota) > $max_reassign) {
			$quota = $max_reassign - $reassigned;
		}
		if ($quota <= 0) {
			echo "voti ($reassigned) gia' tutti riassegnati. Interrompo\n\n";
			break; 
		}		
		$reassigned += $quota;
		echo $debuglog;
		
		$ballot_candidates = explode(",", $r["ballot_data"]);
		$reassign_to = null;		
		echo "cerco a chi riassegnare: ";
		foreach($ballot_candidates AS $c) {
			if ($c != $candidate_id)
				echo $candidate_data[$c]["last_name"]." ";
			if (array_search($c, $stv_done) !== FALSE || $c == $candidate_id) { continue; }
			$reassign_to = $c;
			break;
		}
		if (!$reassign_to) {
			echo "\n\tnon posso riassegnare queste $quota schede - non hanno un secondo/terzo/quarto... nome eleggibile, sono gi� stati tutti scartati o eletti. Schede scartate.\n";
			continue;
		}
		$to_user = $candidate_data[$reassign_to];		
		echo "\n\tpreparo riassegnazione scheda di $from_user[last_name]: $quota ($votes prima della quota proporzionale, drooop $droop) a $to_user[last_name] - progressivo $reassigned su $max_reassign schede\n";
		dbquery("UPDATE poll_votes SET transferred_to=".e($reassign_to)." WHERE ballot_data=".e($r["ballot_data"]));
		if (isset($reassign[$reassign_to])) { $reassign[$reassign_to] += $quota; }
		else { $reassign[$reassign_to] = $quota; }
	}
	# elimina le extra schede fatte fuori dal rounding
	dbquery("UPDATE poll_votes SET transferred_to=1 WHERE poll_id=$poll_id AND candidate_id=$candidate_id AND position=1 AND transferred_to=0");
	return $reassign;

}


function calculate_stv($poll) {
	$poll_id = $poll["id"];
	dbquery("UPDATE poll_votes SET transferred_to = 0 WHERE poll_id=$poll_id");
	
	global $candidate_data;
	$candidate_data = array();
	$q = dbquery("SELECT * FROM poll_candidates WHERE id IN (SELECT DISTINCT candidate_id FROM poll_polls_candidates WHERE poll_id=$poll_id)");
	while ($r = $q->fetch_array(MYSQLI_ASSOC)) {
		$candidate_data[$r[id]] = $r;
	}
	
	$num_votes = dbget("SELECT COUNT(*) AS tot FROM poll_votes WHERE poll_id=$poll_id AND position = 1");	
	$num_votes = intval($num_votes["tot"]);
	$num_seats = intval($poll["seats"]);	
	$droop = floor($num_votes / ($num_seats+1)) + 1;
	echo "DROOP: $droop\n";

	$elected = $stv_done = $candidates = array();
	$elected_ids = array();
	# All'inizio, calcola le prime posizioni
	$q = dbquery("SELECT candidate_id, COUNT(*) AS votes FROM poll_votes WHERE poll_id=$poll_id AND position=1 GROUP BY candidate_id ORDER BY votes DESC");
	while ($r = $q->fetch_array(MYSQLI_ASSOC)) {
		$k = $r["candidate_id"];
		$candidates[$k] = intval($r["votes"]);		
		if ($candidates[$k] >= $droop) {
			$stv_done[] = $k;
			$elected[] = dbget("SELECT *, ".$r["votes"]." AS votes, ".intval($droop)." AS droop FROM poll_candidates WHERE id=$k");
			$elected_ids[] = $k;
		}		
	}
	echo "ELETTI DOPO IL PRIMO ROUND: ".print_r($elected, True)."\n";
	/* Troppi pochi candidati, eleggili tutti */
	if (count($candidates) <= intval($poll["seats"])) {		
		echo "CANDIDATI INSUFFICIENTI - ELEGGO TUTTI";
		foreach($candidates AS $k => $cur_votes) {		
			$elected[] = dbget("SELECT *, ".intval($cur_votes)." AS votes, ".intval($droop)." AS droop FROM poll_candidates WHERE id=$k");
		}
		return $elected;
	}
	
	# Ora comincia a trasferire voti a ruota libera
	echo "\n\n";
	while (count($elected) < $num_seats) {
		# Ora comincia a trasferire voti a ruota libera
		foreach($stv_done AS $k) {
			$reassign = stv_transfer_votes($droop, $poll_id, $k, $stv_done, $candidates);			
			foreach($reassign AS $k2 => $num_votes) {
				$to_user = $candidate_data[$k2];
				echo "Riasegno $num_votes a $to_user[last_name] => che passa da ";
				if (isset($candidates[$k2])) {
					echo intval($candidates[$k2]);
					$candidates[$k2] += $num_votes;
				} else {
					echo "0";
					$candidates[$k2] = $num_votes;
				}
				echo " a ".$candidates[$k2]." voti\n";
			}					
		}
		echo "\n\nclassifica dopo la riassegnazione\n";
		arsort($candidates); $i = 0;
		foreach($candidates AS $k => $cur_votes) {
			if (array_search($k, $elected_ids) !== FALSE) { continue; }
			$i++;			
			print "$i: ".$candidate_data[$k]["last_name"]." => $cur_votes voti\n"; 
		}
		echo "\n\n";
		
		# nuovo round per controllare i nuovi eletti	
		$new_elected = array();		
		foreach($candidates AS $k => $cur_votes) {
			# Gia' eletto, non processare
			if (array_search($k, $elected_ids) !== FALSE) { continue; }
			if ($candidates[$k] >= $droop) {
				$stv_done[] = $k;
				$new_elected[] = $k;
				$elected_ids[] = $k;
				$u = dbget("SELECT *, ".intval($cur_votes)." AS votes, ".intval($droop)." AS droop FROM poll_candidates WHERE id=$k");			
				$elected[] = $u;
				if ($candidates[$k] >= $droop) {
					dbquery("UPDATE poll_votes SET transferred_to=1 WHERE position=1 AND candidate_id=".e($k));
				}
				echo "NUOVO ELETTO DOPO IL TRASFERIMENTO: ".print_r($u, True)."\n\n";					
			}
		}
		if (count($new_elected)) { 				
			continue;
		}
		echo "Scarto candidato con meno voti e li riassegno: ";
		# se non ci sono nuovi eletti, scarta quello con meno voti e riassegna i suoi voti
		asort($candidates);
		$tie_breakers = array();
		$stv_done_start_size = count($stv_done);
		foreach($candidates AS $k => $cur_votes) {
			# voti gi� trasferiti. Vai via.
			if (array_search($k, $stv_done) !== FALSE) { continue; }
			if (!$tie_breakers) {
				$tie_breakers[$k] = $cur_votes;
			}				
			else if ($tie_breakers AND array_search($cur_votes, $tie_breakers) !== FALSE) {
				$tie_breakers[$k] = $cur_votes;
			}
		}
		if (!count($tie_breakers)) {
			echo " nessun candidato scartabile trovato.\n";
			#die("Errore: nessun tie trovato!?"); 
			arsort($candidates);
			foreach($candidates AS $k => $cur_votes) {
				# Gia' eletto, non processare
				if (array_search($k, $elected_ids) !== FALSE) { continue; }				
				$u = dbget("SELECT *, ".intval($cur_votes)." AS votes, ".intval($droop)." AS droop FROM poll_candidates WHERE id=$k");			
				$elected[] = $u;
				echo "ELETTO DOPO TIE FINALE: ".print_r($u, True)."\n\n";		
				if (count($elected) == $num_seats) { break; }
			}
		}
		if (count($tie_breakers) == 1) {
			foreach($tie_breakers AS $k => $cur_votes) {
				$stv_done[] = $k;
				echo " ".$candidate_data[$k]["last_name"]." ($k)";
			}
			echo "\n";
		} else {
			$discarded = dbget("SELECT id FROM poll_candidates WHERE id IN (".e(array_keys($tie_breakers)).") ORDER BY birthdate ASC");
			$stv_done[] = $discarded["id"];
			echo " ".$discarded["id"]."\n";
		}
		# perfetto, ora DOBBIAMO avere un nuovo stv da fare
		if (count($stv_done) == $stv_done_start_size) { 
			die("POSSIBILE ERRORE: nessun tie breaker gestito\n"); 
		}

	}
	
	return $elected;

	
}


function print_results($poll, $elected) {
	$i = 1;
	$txt = "$poll[name] ($poll[seats] eletti)\t\t\t\t\t\t$poll[id]\t\n";
	foreach ($elected as $e) {
		$txt .= "$poll[name]\t$i\t".str_pad("$e[first_name] $e[last_name]", 35)."\t".str_pad("$e[email]", 30)."\t$e[votes]\t$e[id]\t$e[droop]\t$e[party]\n";
		$i++;
	}
	$txt .= "\n\n\n";
	$fp = fopen("results.txt", "a");
	fwrite($fp, $txt);
	fclose($fp);
	print $txt;
}

function calculate_results($poll_ids=null) {
	$SQL = ($poll_ids) ? "WHERE id IN (".e($poll_ids).")" : "";
	$q = dbquery("SELECT * FROM poll_polls $SQL ORDER BY position ASC, id ASC");
	while ($p = $q->fetch_array(MYSQLI_ASSOC)) {
		$seats = intval($p["seats"]);
		print "Processo $p[name]\n==============================\n";		
		if ($seats <= 1) { 
			$elected = calculate_alternative_vote($p);
		} else {		
			$elected = calculate_stv($p);
		}
		print_results($p, $elected);		
	}
}

$options = getopt("p:r:");
if ($options && isset($options["p"]) && $options["p"]) {
	calculate_results($options["p"]);
} else if ($options && isset($options["r"]) && $options["r"]) {
	rebuild_votes_table();
} else {
	calculate_results();
}



