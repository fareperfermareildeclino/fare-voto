<?php
/*
This file is part of FareVoto.
Copyright (C) 2013 Filippo Caprioli - Fare per Fermare il Declino

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
version 2 as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, If not, see <http://www.gnu.org/licenses/>
*/

error_reporting(E_ERROR | E_PARSE);
define("FAREVOTO", True);
/* Impedisci al browser e ai proxy di cacheare la pagina */

require_once("common.php");

function show_votes($poll) {
	if ($poll["secret_ballot"]) { error(_("I dati di questa votazione non sono pubblici"), True); }
	$poll_id = $poll["id"];
	show_header("I voti di ".$poll["name"], "Voto pubblico: ".$poll["name"]);
	echo '<table><tr><th>Elettore</th><th>Ha votato</th><th colspan="10"></th></tr>';

	$q = dbquery("SELECT t.*, pt.* FROM poll_tickets AS t JOIN poll_polls_tickets AS pt ON t.id = pt.ticket_id WHERE pt.poll_id=$poll_id AND t.is_debug=0 ORDER BY t.last_name ASC");	
	$i = 0;
	while ($r = $q->fetch_array(MYSQLI_ASSOC)) {
		$bg = ($i%2) ? "odd" : "even";	
		echo '<tr class="'.$bg.'"><td>'.ucfirst($r["first_name"])."&nbsp;".ucfirst($r["last_name"])."</td>";
		$q2 = dbquery("SELECT c.*, v.ballot_id, v.position FROM poll_candidates AS c JOIN poll_votes AS v ON c.id = v.candidate_id WHERE v.poll_id=$poll_id AND v.ticket_id = ".$r["id"]." ORDER BY position ASC");	
		while ($r2 = $q2->fetch_array(MYSQLI_ASSOC)) {
			if ($r2["is_blank"]) {
				echo '<td>Scheda bianca</td>';
				break;
			} else {
				echo '<td nowrap="nowrap"><b>'.$r2["position"].'</b>.&nbsp;'.ucfirst($r2["first_name"])."&nbsp;".ucfirst($r2["last_name"])."<br><i>(".$r2["party"].")</i></td>";
			}
		}
		echo '</tr>';
		$i++;
	}
	echo "</table>\n";
	echo '<br><br><center><a class="btn grey" href="votes.php">Scegli un\'altra elezione</a></center>';
	show_footer();
}


function show_votes_count($poll) {
	$poll_id = $poll["id"];
	show_header("Statistiche di voto", "Statistiche di voto: ".$poll["name"]);
	$max_position = dbget("SELECT MAX(position) AS pos FROM poll_votes WHERE poll_id=$poll_id");
	$max_position = intval($max_position["pos"]);
	if ($max_position > 3) $max_position = 3;
	
	echo '<table><tr><th>Candidato</th>';
	for ($i = 1; $i <= $max_position; $i++) { echo "<th>Voti in $i posizione</th>"; }
	echo '</tr>';
	$blank = dbget("SELECT COUNT(*) AS tot FROM poll_votes WHERE poll_id=$poll_id AND is_blank=1");	

	$q = dbquery("SELECT c.*, pc.* FROM poll_candidates AS c JOIN poll_polls_candidates AS pc ON c.id = pc.candidate_id WHERE pc.poll_id=$poll_id ORDER BY c.last_name ASC");
	$i = 0;
	while ($r = $q->fetch_array(MYSQLI_ASSOC)) {
		$bg = ($i%2) ? "odd" : "even";
		echo '<tr class="'.$bg.'"><td nowrap="nowrap">'.ucfirst($r["first_name"])." ".ucfirst($r["last_name"])."</td>";
		for ($ii = 1; $ii <= $max_position; $ii++) { 
			$t = dbget("SELECT COUNT(*) AS tot FROM poll_votes WHERE poll_id=$poll_id AND candidate_id=".e($r["id"])." AND position = $ii");
			echo "<td align=\"center\">".$t["tot"]."</td>";
		}
		echo '</tr>';
		$i++;
	}
	$bg = ($i%2) ? "odd" : "even";
	echo "<tr class=\"$bg\"><td>Schede Bianche</td><td colspan=\"$max_position\">".$blank["tot"]."</td></tr>";
	echo "</table>\n";
	echo '<br><br><center><a class="btn grey" href="votes.php">Scegli un\'altra elezione</a></center>';
	show_footer();
}


function show_polls() {
	show_header("Statistiche dei voti", "Scegli quale elezione visualizzare");
	$now = time();
	
	echo '<table id="select_poll"><tr><th>Elezione</th><th>Dal</th><th>Al</th><th>Elettori</th><th>Voti</th><th>Bianche</th><th>Candidati</th></tr>';

	$q = dbquery("SELECT * FROM poll_polls WHERE is_debug=0 ORDER BY end_date DESC, name ASC");
	$i = 0;
	while ($r = $q->fetch_array(MYSQLI_ASSOC)) {
		$bg = ($i%2) ? "odd" : "even";
		if (strtotime($r["end_date"]) > $now) {
			echo "<tr class=\"$bg\"><td>".$r["name"]."</td><td>".display_date($r["start_date"])."</td><td>".display_date($r["end_date"])."</td><td></td><td colspan='4'>Votazione non ancora conclusa</td></tr>";
			continue;
		} 
		echo '<tr class="'.$bg.'"><td><a href="votes.php?poll_id='.$r["id"].'">'.$r["name"]."</a></td>";
		echo "<td>".display_date($r["start_date"])."</td><td>".display_date($r["end_date"])."</td>";
		$idx = $r["id"];
		$p = dbget("SELECT COUNT(*) AS tot FROM poll_polls_tickets WHERE poll_id=$idx");
		echo "<td align=\"center\">".$p["tot"]."</td>";
		$p = dbget("SELECT COUNT(*) AS tot FROM poll_votes WHERE poll_id=$idx AND position=1");
		echo "<td align=\"center\">".$p["tot"]."</td>";
		$p = dbget("SELECT COUNT(*) AS tot FROM poll_votes WHERE poll_id=$idx AND is_blank=1");
		echo "<td align=\"center\">".$p["tot"]."</td>";		
		$p = dbget("SELECT COUNT(*) AS tot FROM poll_polls_candidates WHERE poll_id=$idx");
		echo "<td align=\"center\">".$p["tot"]."</td>";				
		echo '</tr>';
		$i++;
	}
	echo "</table>\n";
	
	show_footer();
} 




$poll_id = isset($_GET["poll_id"]) ? intval($_GET["poll_id"]) : null;
if ($poll_id) {
	$poll = dbget("SELECT * FROM poll_polls WHERE id=$poll_id");
	if (!$poll) error("Votazione $poll_id non trovata");
	else if (strtotime($poll["end_date"]) > time()) { error("Votazione $poll[name] non ancora conclusa"); }
	else if ($poll["secret_ballot"]) { show_votes_count($poll); }
	else { show_votes($poll); }
} else {
	show_polls();
}



